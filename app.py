import numpy as np
import pandas as pd
import pickle

from flask import Flask
from flask import request, jsonify, render_template

app = Flask(__name__)

@app.route('/')
def main():
	return render_template('index.html')

model = pickle.load(open('model/model.pkl','rb'))

@app.route('/predict', methods=['POST'])
def predict():
	input_features = [int(x) for x in request.form.values()]
	fianl_features = [np.array(input_features)]
	prediction = model.predict(fianl_features)
	output = round(prediction[0],2)
	return render_template('index.html', prediction_text = 'Sales should be ${}'.format(output))

@app.route('/results', methods=['POST'])
def results():
	data = request.get_json(force=True)
	prediction = model.predict([np.array(list(data.values()))])
	output = prediction[0]
	return jsonify(output)

if __name__ == '__main__' :
	app.run(debug=True)